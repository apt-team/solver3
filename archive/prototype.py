# Silly prototype without backtracking.
# (C) 2023 Julian Andres Klode
# SPDX-License-Identifier: GPL-2.0+
import apt_pkg
import collections


apt_pkg.init()
c = apt_pkg.Cache()
d = apt_pkg.DepCache(c)


class UniqueQueue:
    def __init__(self, queue=[]):
        self.dict = collections.OrderedDict(((type(item), item.id), item) for item in queue)
    def push(self, item):
        self.dict[type(item), item.id] = item

    def pop(self, item=None):
        if item is not None:
            return self.dict.pop((type(item), item.id), None)
        key, item = self.dict.popitem(last=False)
        return item
    def __iter__(self):
        return iter(self.dict.values())
    def __contains__(self, item):
        return (type(item), item.id) in self.dict
    def __bool__(self):
        return bool(self.dict)
    def __len__(self):
        return len(self.dict)
    def copy(self):
        return type(self)(self)


class ConflictError(Exception):
    pass

requests = []
needs = UniqueQueue()
wants = UniqueQueue()
rejects = UniqueQueue()

for p in c.packages:
    if p.current_ver is None:
        continue
    if d.is_auto_installed(p):
        wants.push(d.get_candidate_ver(p))
    else:
        needs.push(d.get_candidate_ver(p))

for r in requests:
    needs.push(r)
for r in rejects:
    needs.pop(r)
queue = UniqueQueue(needs)

for need in needs:
    for _, groups in need.depends_list.items():
            for group in groups:
                all_targets = []
                for dep in group:
                    targets = dep.all_targets()
                    for target in targets:
                        if dep.dep_type_enum in (apt_pkg.Dependency.TYPE_CONFLICTS, apt_pkg.Dependency.TYPE_DPKG_BREAKS):
                            if target in needs:
                                raise ConflictError(dep)
                            else:
                                rejects.push(target)

while queue:
    item = queue.pop()

    if isinstance(item, apt_pkg.Package):
        cand = d.get_candidate_ver(item)
        queue.push(cand)
        wants.add(cand)
        wants.add(item.current_ver)
    if isinstance(item, apt_pkg.Version):
        if item in rejects:
            raise ConflictError(item)

        # Push needs and rejects
        needs.push(item)
        for _, groups in item.depends_list.items():
            for group in groups:
                all_targets = []
                for dep in group:
                    targets = dep.all_targets()
                    for target in targets:
                        if dep.dep_type_enum in (apt_pkg.Dependency.TYPE_CONFLICTS, apt_pkg.Dependency.TYPE_DPKG_BREAKS):
                            if target in needs:
                                raise ConflictError(f"{item} {dep} {target}")
                            else:
                                rejects.push(target)
        # Resolve positive depends
        for _, groups in item.depends_list.items():
            for group in groups:
                all_targets = []
                for dep in group:
                    if dep.dep_type_enum not in (apt_pkg.Dependency.TYPE_DEPENDS, apt_pkg.Dependency.TYPE_PREDEPENDS, apt_pkg.Dependency.TYPE_RECOMMENDS):
                        continue
                    targets = dep.all_targets()
                    for target in targets:

                        if target in rejects:
                            continue
                        if any(t.id == target.id for t in all_targets):
                            continue
                        all_targets.append(target)
                if dep.dep_type_enum not in (apt_pkg.Dependency.TYPE_DEPENDS, apt_pkg.Dependency.TYPE_PREDEPENDS, apt_pkg.Dependency.TYPE_RECOMMENDS):
                    continue
                if dep.dep_type_enum in (apt_pkg.Dependency.TYPE_RECOMMENDS, ):
                    for target in all_targets:
                        if target not in needs:
                            wants.push(target)
                    continue
                if item.parent_pkg.name == "thunderbird-gnome-support":
                    print(all_targets)
                if not all_targets:
                    raise ValueError("No choices for " + str(group) + " in " + str(item))
                for target in all_targets:
                    if target in needs:
                        break
                    if target in wants or len(all_targets) == 1:
                        queue.push(target)
                        break
                else:
                    raise ValueError("Need to pick for " + str(group) + " amongst " + str(all_targets))


print("Installs", len(needs))

with apt_pkg.ActionGroup(d):
    for p in c.packages:
        cand = d.get_candidate_ver(p)
        if (d.marked_install(p) or (p.current_ver is not None and d.marked_keep(p))) and d.get_candidate_ver(p) not in needs:
            #print("Delete", p)
            d.mark_delete(p)

    for need in needs:
        if need in rejects:
          print("Error", need, "is in rejects")
        d.set_candidate_ver(need.parent_pkg, need)
        d.mark_install(need.parent_pkg, False)

print("Install", d.inst_count)
print("Keep", d.keep_count)
print("Delete", d.del_count)
print("Broken", d.broken_count)

for p in sorted(c.packages, key=lambda p: p.name):
    if (d.marked_install(p) or (p.current_ver is not None and d.marked_keep(p))):
        if d.get_candidate_ver(p) not in needs:
            print("Unexpected install", p)
        print(p.name + "=" + d.get_candidate_ver(p).ver_str)
    if d.is_inst_broken(p):
        print("Broken:", p)

