This is the APT 3.0 solver research project.

The paper describing the solver lives in doc/ and is licensed
under CC-BY 4.0 international.

The solver code lives in src/ and is licensed under GPL-2.0+.
